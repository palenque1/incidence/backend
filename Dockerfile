# We use the official PHP image with the desired tag
FROM php:8.1-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install the necessary dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libonig-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    git \
    curl

# Install required dependencies
RUN apt-get update && apt-get install -y \
    libpq-dev \
    libzip-dev \
    libonig-dev \
    libexif-dev \
    && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo pdo_pgsql zip exif pcntl
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

# Working directory inside the container
WORKDIR /var/www/

# Copy composer.json and composer.lock to allow installation of dependencies
COPY composer.json composer.lock  /var/www/

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# We copy the rest of the application files
COPY .  /var/www/

# # We generate the Laravel application key
# RUN php artisan key:generate


#RUN useradd -G www-data,root -u $uid -d /home/$user $user
#RUN mkdir -p /home/$user/.composer && \
#    chown -R $user:$user /home/$user

# Command to start the PHP-FPM server
CMD ["php-fpm"]
