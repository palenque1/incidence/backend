<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).



# incidence-api Installation Guide

Follow these steps to set up and run the `incidence-api` project on port 8010:

## Prerequisites

1. **Docker Installation**:
   - Install Docker from the official website: [Docker Official Website](https://www.docker.com/get-started)
   - Ensure Docker Compose is also installed. It is typically included with Docker on most platforms.

2. **Install Docker Compose** (if not already installed):
   - If `docker-compose` is not automatically installed with Docker, you can download and install it using the following steps:
   
     - **Linux**:
       1. Go to the official `docker-compose` GitHub releases page: [docker-compose GitHub](https://github.com/docker/compose/releases).
       2. Find the latest stable release and download the `docker-compose-Linux-x86_64` file.
       3. Move the downloaded file to `/usr/local/bin`:
          ```bash
          sudo mv docker-compose-Linux-x86_64 /usr/local/bin/docker-compose
          ```
       4. Make it executable:
          ```bash
          sudo chmod +x /usr/local/bin/docker-compose
          ```
     - **macOS**:
       1. Go to the official `docker-compose` GitHub releases page: [docker-compose GitHub](https://github.com/docker/compose/releases).
       2. Find the latest stable release and download the `docker-compose-Darwin-x86_64` file.
       3. Move the downloaded file to a directory that's included in your system's PATH.
       4. Make it executable, if needed.
       
     - **Windows**:
       1. Download the `docker-compose-Windows-x86_64.exe` file from the official GitHub releases page.
       2. Place the downloaded file in a directory that's included in your system's PATH.


## Configuration

3. **Environment Variables**:
   - Copy the example environment file to a new `.env` file using the following command:
     ```
     cp .env.example .env
     ```
   - Edit the `.env` file and set the necessary environment variables required for your `incidence-api` setup.

## Running the Application

4. **Start Incidence API**:
   - Run the following command to start the incidence API using Docker on port 8010:
     ```
     ./docker-incidence deploy
     ```
   - Make sure the script (`docker-incidence`) has executable permissions. If not, you can set the permissions using:
     ```
     chmod +x docker-incidence
     ```

Now, you should have `incidence-api` up and running on port 8010 with the necessary configuration. Make sure to consult the project documentation for any additional setup or usage instructions.

