<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticatedSessionController extends Controller
{
    /**
     * Handle an incoming authentication request.
     */
    public function store(Request $request)
    {
        // First, verify if the email exists in the database
        $user = User::where('email', $request['email'])->first();

        // If the user doesn't exist, throw an error
        if (!$user) {
            throw new \Exception('Email not found.', 404);
        }
        // If the user exists, check if the password is correct using Laravel's Auth
        if (!Hash::check($request['password'], $user->password)) {
            throw new \Exception('Incorrect password.', 401);
        }

        $credentials = $request->only('email', 'password');
        $username = $request->input('email');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = JWTAuth::customClaims([
                'email' => $user->email,
                'user_id' => $user->id,
                'name' => $user->name,
                'rol' => $user->rol,
            ])->attempt($credentials);
            return ['message' => 'Successful', 'user' => $user, 'token' => $token];
        }

        return response()->json([
            'error' => true,
            'message' => 'Authentication failed'
        ], 401);

    }

    public function refreshToken()
    {
        try {
            // Attempt to refresh the token
            $newToken = JWTAuth::parseToken()->refresh();
            return response()->json(['token' => $newToken]);
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'Could not refresh token'], 500);
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request)
    {
        try {
            // Get the token from the request
            \Log::info("Request:");
            $token = $request->bearerToken();
            \Log::info($token);

            // Invalidate the token
            JWTAuth::setToken($token)->invalidate(true);
            \Log::info("End");

            return response()->json(['message' => 'User successfully signed out'], Response::HTTP_OK);
        } catch (\Exception $e) {
            // Return a response in case of an error during invalidation
            return response()->json(['message' => 'Failed to logout, please try again', 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
