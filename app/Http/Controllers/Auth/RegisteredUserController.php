<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisteredUserController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        // If validation passes, create the user
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
            'rol' => 'REVIEWER'
        ]);

        event(new Registered($user));

        // Generate the token
        $token = JWTAuth::fromUser($user);

        // Return the token in the response
        return response()->json([
            'message' => 'Registration successful',
            'user' => $user,
            'token' => $token,
        ], Response::HTTP_CREATED);

        return response()->json(['message' => 'Registration successful'], 201);
    }
}
