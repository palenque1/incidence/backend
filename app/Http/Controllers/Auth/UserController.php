<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();
        return response()->json($users);
    }
    /**
     * Handle an incoming authentication request.
     */
    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        $user = Auth::user();

        // If the user exists, check if the password is correct using Laravel's Auth
        if (!Hash::check($request['current_password'], $user->password)) {
            return response()->json(['message' => 'Current password does not match our records.'], 400);
        }

        // Check if the new password is different from the current password
        if (Hash::check($request->new_password, $user->password)) {
            return response()->json(['message' => 'New password must be different from the current password.'], 400);
        }

        // Update the user's password
        $user->password = Hash::make($request->new_password);
        $user->save();

        return response()->json(['message' => 'Password successfully changed.'], 202);
    }

    public function changeRol(Request $request, $userId)
    {
        $request->validate(['rol' => 'required|string']);

        $user = User::findOrFail($userId);

        $user->rol = $request->rol;
        $user->save();

        return response()->json(['message' => 'Rol successfully changed.'], 202);
    }
}
