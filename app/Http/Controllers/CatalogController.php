<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Catalog;
use Carbon\Carbon;

class CatalogController extends Controller
{
    public function index()
    {
        $data = Catalog::all();

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $item = new Catalog($data);
        $item->save();

        return response()->json(['message' => 'Item added successfully'], 201);
    }

    public function destroy($value)
    {
        $item = Catalog::where('value', $value);
        if (!$item) {
            return response()->json(['message' => 'Item not found'], 404);
        }

        $item->delete();

        return response()->json(['message' => 'Item deleted'], 200);
    }

    public function getLocations()
    {
        return response()->json(Catalog::where('type', 'location')->get(['id', 'value']));
    }

    public function getCauses()
    {
        return response()->json(Catalog::where('type', 'cause')->get(['id', 'value']));
    }

    public function getSources()
    {
        return response()->json(Catalog::where('type', 'source')->get(['id', 'value']));
    }

}
