<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Response;
use App\Rules\EntryValidationRules;

use Illuminate\Http\Request;

class EntryController extends Controller
{
    // Display a listing of the entries.
    public function index($userId, Request $request)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        // Retrieve pagination parameters
        $perPage = $request->query('perPage', 10); // Default 10 items per page
        $page = $request->query('page', 1); // Default to page 1

        // Paginate entries for the user
        $entries = Entry::orderBy('date', 'desc') // Optional: Sort by date descending
            ->paginate($perPage, ['*'], 'page', $page);

        return response()->json($entries, 200);
    }

    // Store a newly created entry in storage.
    public function store(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $validatedData = $request->validate(EntryValidationRules::create());

        $entry = $user->entry()->create($validatedData);

        return response()->json(['message' => 'Entry created successfully', 'entry' => $entry], 201);
    }

    // Display the specified entry.
    public function show($userId, $entryId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $entry = Entry::where('id', $entryId)
            ->firstOrFail();

        if (!$entry) {
            return response()->json(['message' => 'Entry not found'], 404);
        }

        return response()->json($entry, 202);
    }

    // Update the specified entry in storage.
    public function update(Request $request, $userId, $entryId)
    {
        $user = User::find($userId);
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $entry = Entry::where('id', $entryId)
            ->firstOrFail();

        if (!$entry) {
            return response()->json(['message' => 'Entry not found'], 404);
        }

        $validatedData = $request->validate(EntryValidationRules::update());
        $entry->update($validatedData);

        return response()->json(['message' => 'Entry updated successfully', $entry], 205);
    }

    public function destroy($id)
    {
        $id->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function getUpdates($entryId)
    {
        $entry = Entry::findOrFail($entryId);
        return response()->json($entry->updates, 202);
    }

    public function addUpdate(Request $request, $id)
    {
        $entry = Entry::find($id);

        if (!$entry) {
            return response()->json(['message' => 'Entry not found'], 404);
        }

        $validatedData = $request->validate(EntryValidationRules::addUpdates());

        try {
            $update = $entry->updates()->create($validatedData);
            return response()->json(['message' => 'Update added successfully', 'update' => $update], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to add update', 'Error:' => $e], 500);
        }
    }

    public function search(Request $request)
    {
        $searchTerm = $request->get('term');

        $results = Entry::where('location', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('source', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('cause', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('case', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('legal', 'ILIKE', '%' . $searchTerm . '%')
            ->get();

        return response()->json($results, 200);
    }

    public function search2(Request $request)
    {
        $searchTerm = $request->get('term');

        $results = Entry::where('location', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('source', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('cause', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('case', 'ILIKE', '%' . $searchTerm . '%')
            ->orWhere('legal', 'ILIKE', '%' . $searchTerm . '%');

        // Attempt to parse the search term as a date or date range
        try {
            $date = Carbon::createFromFormat('d-m-Y', $searchTerm)->startOfDay();
            $results->orWhereDate('date', '=', $date);
        } catch (\Exception $e) {
            // If it's not a single date, check for a date range
            $dates = explode(' ', $searchTerm);
            if (count($dates) === 2) {
                try {
                    $start = Carbon::createFromFormat('d-m-Y', trim($dates[0]))->startOfDay();
                    $end = Carbon::createFromFormat('d-m-Y', trim($dates[1]))->endOfDay();
                    $results->orWhereBetween('date', [$start, $end]);
                } catch (\Exception $e) {
                    // Not a date range, ignore the exception
                }
            }
        }

        // Execute the query and get the results
        return response()->json($results->get(), 200);
    }
}
