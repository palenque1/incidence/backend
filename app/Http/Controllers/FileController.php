<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Entry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function index($entryId)
    {
        $entry = Entry::findOrFail($entryId);

        return response()->json($entry->files, 201);
    }

    public function store(Request $request, $entryId)
    {
        $request->validate([
            'file' => 'required|file|mimes:pdf,mp4,png,jpg,doc,xlsx|max:40960', // 40MB Max size
        ]);

        $file = $request->file('file');

        // Store the file in the 'public' disk (or any disk you prefer)
        $path = $file->store('uploads', 'public');

        // Create a new file record in the database
        $newFile = new File;
        $newFile->entry_id = $entryId;
        $newFile->mime_type = $file->getClientMimeType();
        $newFile->name = $file->getClientOriginalName();
        $newFile->path = $path;
        $newFile->size = $file->getSize();
        $newFile->extension = $file->getClientOriginalExtension();
        $newFile->save();

        return response()->json([
            'message' => 'File uploaded successfully!',
            'file' => $newFile,
        ], 201);
    }

    public function uploadFiles(Request $request, $entryId)
    {
        if ($request->hasFile('files')) {
            $files = $request->file('files');

            foreach ($files as $index => $file) {
                // Validate each file
                $validatedData = $request->validate([
                    "files[$index]" => 'mimes:pdf,mp4,png,jpg,doc,xlsx|max:102400',
                ]);

                // Store file and get path
                $path = $file->store('uploads', 'public');

                // Insert file information into the database
                $newFile = new File;
                $newFile->entry_id = $entryId;
                $newFile->mime_type = $file->getClientMimeType();
                $newFile->name = $file->getClientOriginalName();
                $newFile->path = $path;
                $newFile->size = $file->getSize();
                $newFile->extension = $file->getClientOriginalExtension();
                $newFile->save();
            }

            return response()->json(['message' => 'Files uploaded successfully!'], 201);
        }

        return response()->json(['message' => 'No files were uploaded.'], 400);
    }

    public function downloadFile($fileId)
    {
        $file = File::findOrFail($fileId);

        $filePath = storage_path('app/public/' . $file->path);

        if (!File::exists($filePath)) {
            abort(404);
        }

        $headers = ['Content-Type' => $file->mime_type];
        return response()->download($filePath, $file->name, $headers);
    }

    public function destroy($id)
    {
        try {
            // Find file by ID
            $file = File::findOrFail($id);

            // Delete the file from storage
            if (Storage::exists($file->path)) {
                Storage::delete($file->path);
            }

            // Delete file record from the database
            $file->delete();

            return response()->json(['message' => 'File deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'File deletion failed: ' . $e], 500);
        }
    }
}
