<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Entry;
use Carbon\Carbon;

class VisualizationController extends Controller
{
    //
    public function getDataPeriods(Request $request)
    {
        try {
            // Validation
            $request->validate([
                'timelineType' => 'required|string',
                'startPeriod' => 'required_if:timelineType,custom|date',
                'endPeriod' => 'required_if:timelineType,custom|date|after_or_equal:startPeriod',
            ]);

            // Determine the timeline type from the request (e.g., 'annual', 'quarterly', 'custom')
            $timelineType = $request->input('timelineType');
            $startPeriod = $request->input('startPeriod'); // For custom period
            $endPeriod = $request->input('endPeriod'); // For custom period

            $entries = [];

            // Modify query based on timeline type
            switch ($timelineType) {
                case 'annual':
                    $entries = Entry::select(
                        DB::raw("EXTRACT(YEAR FROM date) as name"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"))
                        ->get();
                    break;

                case 'quarterly':
                    $entries = Entry::select(
                        DB::raw("EXTRACT(YEAR FROM date) as year"),
                        DB::raw("EXTRACT(QUARTER FROM date) as name"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"), DB::raw("EXTRACT(QUARTER FROM date)"))
                        ->orderBy('name', 'asc')
                        ->get();
                    break;

                case '4weeks':
                    // Define the 4-week periods based on current date or a fixed date range
                    $entries = Entry::select(
                        DB::raw("date as period_start"),
                        DB::raw("date + INTERVAL '28 days' as period_end"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->where('date', '>=', DB::raw("CURRENT_DATE - INTERVAL '28 days'")) // for the last 28 days
                        ->groupBy('date')
                        ->get()
                        ->map(function ($entry) {
                            $periodStart = Carbon::createFromFormat('Y-m-d', $entry->period_start);
                            $periodEnd = $periodStart->copy()->addDays(28);
                            $entry->name = $periodStart->toDateString() . ' to ' . $periodEnd->toDateString();
                            return $entry;
                        });
                    break;

                case 'custom':
                    // Filter by custom date range
                    $entries = Entry::whereBetween('date', [$startPeriod, $endPeriod])
                        ->select(
                            DB::raw('sum(outpayment) as outpayment'),
                            DB::raw('sum(deductible) as deductible')
                        )
                        ->get()
                        ->map(function ($entry) use ($startPeriod, $endPeriod) {
                            $entry->name = $startPeriod . ' to ' . $endPeriod;
                            return $entry;
                        });
                    break;
            }

            return response()->json($entries);
        } catch (\Illuminate\Validation\ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getDataPeriodsWithFilters(Request $request)
    {
        try {
            // Validation
            $request->validate([
                'timelineType' => 'required|string',
                'startPeriod' => 'required_if:timelineType,custom|date',
                'endPeriod' => 'required_if:timelineType,custom|date|after_or_equal:startPeriod',
            ]);

            // Filter parameters
            $location = $request->input('location', null); // Optional filter
            $source = $request->input('source', null); // Optional filter
            $cause = $request->input('cause', null); // Optional filter
            $date = $request->input('date', null); // Optional filter
            $caseStatus = $request->input('case', null); // Optional filter
            $legalStatus = $request->input('legal', null); // Optional filter

            // Determine the timeline type from the request (e.g., 'annual', 'quarterly', 'custom')
            $timelineType = $request->input('timelineType');
            $startPeriod = $request->input('startPeriod'); // For custom period
            $endPeriod = $request->input('endPeriod'); // For custom period

            $query = Entry::query(); // Start with a base query

            // Apply location filter if provided
            if ($location) {
                $query->where('location', $location);
            }
            if ($source) {
                $query->where('source', $source);
            }
            if ($cause) {
                $query->where('cause', $cause);
            }
            if ($date) {
                $query->where('date', $date);
            }
            if ($caseStatus) {
                $query->where('case', $caseStatus);
            }
            if ($legalStatus) {
                $query->where('legal', $legalStatus);
            }

            $entries = [];

            // Modify query based on timeline type
            switch ($timelineType) {
                case 'annual':
                    $entries = $query->select(
                        DB::raw("EXTRACT(YEAR FROM date) as name"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"))
                        ->get();
                    break;

                case 'quarterly':
                    $entries = $query->select(
                        DB::raw("EXTRACT(YEAR FROM date) as year"),
                        DB::raw("EXTRACT(QUARTER FROM date) as name"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"), DB::raw("EXTRACT(QUARTER FROM date)"))
                        ->orderBy('name', 'asc')
                        ->get();
                    break;

                case '4weeks':
                    // Define the 4-week periods based on current date or a fixed date range
                    $entries = $query->select(
                        DB::raw("date as period_start"),
                        DB::raw("date + INTERVAL '28 days' as period_end"),
                        DB::raw("sum(outpayment) as outpayment"),
                        DB::raw("sum(deductible) as deductible")
                    )
                        ->where('date', '>=', DB::raw("CURRENT_DATE - INTERVAL '28 days'")) // for the last 28 days
                        ->groupBy('date')
                        ->get()
                        ->map(function ($entry) {
                            $periodStart = Carbon::createFromFormat('Y-m-d', $entry->period_start);
                            $periodEnd = $periodStart->copy()->addDays(28);
                            $entry->name = $periodStart->toDateString() . ' to ' . $periodEnd->toDateString();
                            return $entry;
                        });
                    break;

                case 'custom':
                    // Filter by custom date range
                    $entries = $query->whereBetween('date', [$startPeriod, $endPeriod])
                        ->select(
                            DB::raw('sum(outpayment) as outpayment'),
                            DB::raw('sum(deductible) as deductible')
                        )
                        ->get()
                        ->map(function ($entry) use ($startPeriod, $endPeriod) {
                            $entry->name = $startPeriod . ' to ' . $endPeriod;
                            return $entry;
                        });
                    break;
            }

            return response()->json($entries);
        } catch (\Illuminate\Validation\ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getTotals(Request $request)
    {
        try {
            // Validation
            $request->validate([
                'timelineType' => 'required|string',
                'startPeriod' => 'required_if:timelineType,custom|date',
                'endPeriod' => 'required_if:timelineType,custom|date|after_or_equal:startPeriod',
            ]);

            // Filter parameters
            $location = $request->input('location', null); // Optional filter
            $source = $request->input('source', null); // Optional filter
            $cause = $request->input('cause', null); // Optional filter
            $date = $request->input('date', null); // Optional filter
            $injured = $request->input('injured', null); // Optional filter
            $reference = $request->input('reference', null); // Optional filter

            // Determine the timeline type from the request (e.g., 'annual', 'quarterly', 'custom')
            $timelineType = $request->input('timelineType');
            $startPeriod = $request->input('startPeriod'); // For custom period
            $endPeriod = $request->input('endPeriod'); // For custom period

            $query = Entry::query(); // Start with a base query

            // Apply location filter if provided
            if ($location) {
                $query->where('location', $location);
            }
            if ($source) {
                $query->where('source', $source);
            }
            if ($cause) {
                $query->where('cause', $cause);
            }
            if ($date) {
                $query->where('date', $date);
            }
            if ($injured) {
                $query->where('injured', $injured);
            }
            if ($reference) {
                $query->where('reference', $reference);
            }

            $entries = [];

            // Modify query based on timeline type
            switch ($timelineType) {
                case 'annual':
                    $entries = $query->select(
                        DB::raw("EXTRACT(YEAR FROM date) as name"),
                        DB::raw("COUNT(*) as entries")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"))
                        ->orderBy('name', 'asc')
                        ->get();
                    break;

                case 'quarterly':
                    $entries = $query->select(
                        DB::raw("EXTRACT(YEAR FROM date) as year"),
                        DB::raw("EXTRACT(QUARTER FROM date) as name"),
                        DB::raw("COUNT(*) as entries")
                    )
                        ->groupBy(DB::raw("EXTRACT(YEAR FROM date)"), DB::raw("EXTRACT(QUARTER FROM date)"))
                        ->orderBy('name', 'asc')
                        ->get();
                    break;

                case '4weeks':
                    // Define the 4-week periods based on current date or a fixed date range
                    $entries = $query->select(
                        DB::raw("date as period_start"),
                        DB::raw("date + INTERVAL '28 days' as period_end"),
                        DB::raw("COUNT(*) as entries")
                    )
                        ->where('date', '>=', DB::raw("CURRENT_DATE - INTERVAL '28 days'")) // for the last 28 days
                        ->groupBy('date')
                        ->orderBy('asc')
                        ->get()
                        ->map(function ($entry) {
                            $periodStart = Carbon::createFromFormat('Y-m-d', $entry->period_start);
                            $periodEnd = $periodStart->copy()->addDays(28);
                            $entry->name = $periodStart->toDateString() . ' to ' . $periodEnd->toDateString();
                            return $entry;
                        });
                    break;

                case 'custom':
                    // Filter by custom date range
                    $entries = $query->whereBetween('date', [$startPeriod, $endPeriod])
                        ->select(
                            DB::raw("COUNT(*) as entries")
                        )
                        ->get()
                        ->map(function ($entry) use ($startPeriod, $endPeriod) {
                            $entry->name = $startPeriod . ' to ' . $endPeriod;
                            return $entry;
                        });
                    break;
            }

            return response()->json($entries);
        } catch (\Illuminate\Validation\ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getDataForAll(Request $request)
    {
        try {
            // Validation
            $request->validate([
                'timelineType' => 'required|string',
                'startPeriod' => 'required_if:timelineType,custom|date',
                'endPeriod' => 'required_if:timelineType,custom|date|after_or_equal:startPeriod',
                'year' => 'required_if:timelineType,annual,quarterly|integer'
            ]);

            $timelineType = $request->input('timelineType');
            $startPeriod = $request->input('startPeriod');
            $endPeriod = $request->input('endPeriod');
            $year = $request->input('year');

            $query = Entry::query();

            // Filter parameters
            $filters = [
                'location' => $request->input('location'),
                'source' => $request->input('source'),
                'cause' => $request->input('cause'),
                'date' => $request->input('date'),
                'injured' => $request->input('injured'),
                'reference' => $request->input('reference'),
            ];

            foreach ($filters as $key => $value) {
                if ($value !== null) {
                    $query->where($key, $value);
                }
            }

            // Apply timeline based filtering
            switch ($timelineType) {
                case 'custom':
                    if ($startPeriod && $endPeriod) {
                        $page = $request->input('page', 1);
                        $perPage = $request->input('perPage', 10);
                        $locationIds = $request->input('locations');
                        $causeIds = $request->input('causes');
                        $sourceIds = $request->input('sources');
                        $customData = $this->getCustomPeriodData($query, $startPeriod, $endPeriod, $page, $perPage, $locationIds, $causeIds, $sourceIds);
                        return response()->json($customData);
                    }
                    break;
                case 'annual':
                    if ($year) {
                        // info('Annual filter applied for year: ' . $year);
                        $page = $request->input('page', 1);
                        $perPage = $request->input('perPage', 10);
                        $comparisonYear = $request->input('comparisonYear');
                        $locationIds = $request->input('locations');
                        $causesIds = $request->input('causes');
                        $sourceIds = $request->input('sources');
                        $years = [$year, $comparisonYear];
                        $annualData = $this->getAnnualData($query, $page, $perPage, $years, $locationIds, $causesIds, $sourceIds);
                        return response()->json($annualData);
                    }
                    break;
                case 'quarterly':
                    if ($year) {
                        $page = $request->input('page', 1);
                        $perPage = $request->input('perPage', 10);
                        $quarterPage = $request->input('secondPage', 1);
                        $comparisonYear = $request->input('comparisonYear');
                        $years = [$year, $comparisonYear];
                        $locationIds = $request->input('locations');
                        $causesIds = $request->input('causes');
                        $sourceIds = $request->input('sources');
                        $dataByQuarter = $this->getQuarters(
                            $query,
                            $years,
                            $quarterPage,
                            $page,
                            $perPage,
                            $locationIds,
                            $causesIds,
                            $sourceIds
                        );
                        return response()->json($dataByQuarter);
                    }
                    break;
                case '13periods':
                    if ($year) {
                        $page = $request->input('page', 1);
                        $perPage = $request->input('perPage', 10);
                        $periodPage = $request->input('secondPage', 1);
                        $comparisonYear = $request->input('comparisonYear');
                        $years = [$year, $comparisonYear];
                        $locationIds = $request->input('locations');
                        $causesIds = $request->input('causes');
                        $sourceIds = $request->input('sources');
                        $dataByPeriod = $this->get13Periods(
                            $query,
                            $years,
                            $periodPage,
                            $page,
                            $perPage,
                            $locationIds,
                            $causesIds,
                            $sourceIds
                        );
                        return response()->json($dataByPeriod);
                    }
                    break;
            }

            $entries = $query->get();

            // Calculate totals and proportions
            $totalEntries = count($entries);
            // $totals = ['total_entries' => $totalEntries];
            $proportions = [];

            // Calculate proportions for unfiltered fields
            foreach (['location', 'source', 'cause'] as $field) {
                if (empty($filters[$field])) {
                    $counts = $entries->groupBy($field)->map->count();
                    $topThree = $counts->sortDesc()->take(3);

                    $proportions[$field] = $topThree->map(function ($count, $key) use ($totalEntries) {
                        return [
                            'name' => $key,
                            'entries' => $count,
                            'percentage' => round(($count / $totalEntries) * 100, 2) . '%'
                        ];
                    })->values();
                }
            }

            return response()->json([[
                'totals' => $totalEntries,
                'proportions' => $proportions
            ]]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getQuartersOld($year, $queryBase)
    {
        try {
            $dataByQuarter = [];

            // Initial start date for 2024
            $initialStartDate2024 = Carbon::create(2024, 1, 3);

            // Calculate the initial start date for the given year
            if ($year >= 2024) {
                $yearsDifference = $year - 2024;
                $initialStartDate = $initialStartDate2024->copy()->addYears($yearsDifference);
            } else {
                $yearsDifference = 2024 - $year;
                $initialStartDate = $initialStartDate2024->copy()->subYears($yearsDifference);
            }

            // Adjust the initial start date based on the 13 periods per year and leap years
            $daysDifference = ($yearsDifference * 52 * 7);
            if ($year >= 2024) {
                $initialStartDate = $initialStartDate2024->copy()->addDays($daysDifference);
            } else {
                $initialStartDate = $initialStartDate2024->copy()->subDays($daysDifference);
            }

            // Calculate start and end dates for each period
            $periods = [];
            for ($i = 0; $i < 13; $i++) {
                $startDate = $initialStartDate->copy()->addWeeks($i * 4);
                $endDate = $startDate->copy()->addWeeks(4)->subDay();
                $periods[] = ['start' => $startDate, 'end' => $endDate];
            }

            // Group periods into quarters
            $quarters = [
                'Q1' => array_slice($periods, 0, 3),
                'Q2' => array_slice($periods, 3, 3),
                'Q3' => array_slice($periods, 6, 3),
                'Q4' => array_slice($periods, 9, 4),
            ];

            // Process data for each quarter
            foreach ($quarters as $quarterName => $periodGroup) {
                $quarterQuery = clone $queryBase;
                $quarterQuery->where(function ($query) use ($periodGroup) {
                    foreach ($periodGroup as $period) {
                        $query->orWhereBetween('date', [$period['start'], $period['end']]);
                    }
                });

                $totalEntries = $quarterQuery->count();

                // Gather proportions for unfiltered fields
                $proportions = [
                    'location' => $this->calculateTopProportions($quarterQuery, 'location'),
                    'source' => $this->calculateTopProportions($quarterQuery, 'source'),
                    'cause' => $this->calculateTopProportions($quarterQuery, 'cause'),
                ];

                // Structure data by quarter
                $dataByQuarter[] = [
                    'quarter' => $quarterName,
                    'totals' => $totalEntries,
                    // 'date' => $date,
                    'proportions' => $proportions
                ];
            }

            return $dataByQuarter;
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getQuarters($queryBase, $years, $quarterPage = 1, $page = 1, $perPage = 5, $locationIds = [], $causeIds = [], $sourceIds = [])
    {
        $year = $years[0];
        $comparisonYear = $years[1];
        try {
            $dataByQuarter = [];

            // Start dates for each quarter in the selected year
            $quarters = [
                'Q1' => [Carbon::create($year, 1, 1), Carbon::create($year, 3, 31)],
                'Q2' => [Carbon::create($year, 4, 1), Carbon::create($year, 6, 30)],
                'Q3' => [Carbon::create($year, 7, 1), Carbon::create($year, 9, 30)],
                'Q4' => [Carbon::create($year, 10, 1), Carbon::create($year, 12, 31)],
            ];

            $quartersComparison = $comparisonYear ? [
                'Q1' => [Carbon::create($comparisonYear, 1, 1), Carbon::create($comparisonYear, 3, 31)],
                'Q2' => [Carbon::create($comparisonYear, 4, 1), Carbon::create($comparisonYear, 6, 30)],
                'Q3' => [Carbon::create($comparisonYear, 7, 1), Carbon::create($comparisonYear, 9, 30)],
                'Q4' => [Carbon::create($comparisonYear, 10, 1), Carbon::create($comparisonYear, 12, 31)],
            ] : null;

            // Process the selected quarter only
            $selectedQuarter = array_slice(array_values($quarters), $quarterPage - 1, 1)[0];
            $startDate = $selectedQuarter[0];
            $endDate = $selectedQuarter[1];

            // Apply filters for the main year
            list($locationNames, $causeNames, $sourceNames) = $this->getCatalogValues($locationIds, $causeIds, $sourceIds);

            $mainQuery = clone $queryBase;
            $mainQuery->whereBetween('date', [$startDate->toDateString(), $endDate->toDateString()]);

            if (!empty($locationNames)) {
                $mainQuery->whereIn('location', $locationNames);
            }
            if (!empty($causeNames)) {
                $mainQuery->whereIn('cause', $causeNames);
            }
            if (!empty($sourceNames)) {
                $mainQuery->whereIn('source', $sourceNames);
            }

            // Fetch main year entries
            $totalEntries = $mainQuery->count();
            $locations = $mainQuery->select(
                'location',  // Fetch the location name directly
                DB::raw("COUNT(*) as totals"),
                DB::raw("$totalEntries as total_entries")
            )
                ->groupBy('location')
                ->orderBy('location', 'asc');

            // Apply pagination to the query if no comparison year
            if (!$comparisonYear) {
                $locations = $locations
                    ->skip(($page - 1) * $perPage)
                    ->take($perPage)
                    ->get();
            } else {
                $locations = $locations->get()->keyBy('location');
            }

            $response = [
                'totals' => $locations,
                'totalPages' => ceil($totalEntries / $perPage),
                'currentPage' => $page,
            ];

            // Fetch comparison data if a comparison year is provided
            $comparisonTotals = $comparisonYear ? collect() : collect([]);
            if ($comparisonYear && $quartersComparison) {
                $comparisonQuarter = array_slice(array_values($quartersComparison), $quarterPage - 1, 1)[0];
                $comparisonStartDate = $comparisonQuarter[0];
                $comparisonEndDate = $comparisonQuarter[1];

                $comparisonQuery = clone $queryBase;
                $comparisonQuery->whereBetween('date', [$comparisonStartDate->toDateString(), $comparisonEndDate->toDateString()]);

                if (!empty($locationNames)) {
                    $comparisonQuery->whereIn('location', $locationNames);
                }
                if (!empty($causeNames)) {
                    $comparisonQuery->whereIn('cause', $causeNames);
                }
                if (!empty($sourceNames)) {
                    $comparisonQuery->whereIn('source', $sourceNames);
                }

                // Fetch comparison entries
                $comparisonTotalEntries = $comparisonQuery->count();
                $comparisonTotals = $comparisonQuery->select(
                    'location',
                    DB::raw("COUNT(*) as totals"),
                    DB::raw("$comparisonTotalEntries as total_entries")
                )
                    ->groupBy('location')
                    ->orderBy('location', 'asc')
                    ->get()
                    ->keyBy('location');

                // Combine both the original and comparison locations into one response
                $allLocations = $locations->keys()->merge($comparisonTotals->keys())->unique()->sort();
                $totalsArray = [];
                $comparisonArray = [];

                // Fill in zeros for missing data
                foreach ($allLocations as $location) {
                    $totalsArray[] = [
                        'location' => $location,
                        'totals' => $locations->has($location) ? $locations[$location]->totals : 0,
                        'total_entries' => $locations->has($location) ? $locations[$location]->total_entries : $totalEntries,
                        'year' => $year,
                    ];

                    $comparisonArray[] = [
                        'location' => $location,
                        'comparisonTotals' => $comparisonTotals->has($location) ? $comparisonTotals[$location]->totals : 0,
                        'total_entries' => $comparisonTotals->has($location) ? $comparisonTotals[$location]->total_entries : 0,
                        'year' => $comparisonYear,
                    ];

                    $totalLocations = count($allLocations);
                    $totalPages = ceil($totalLocations / $perPage);
                    $start = ($page - 1) * $perPage;
                    $paginatedTotals = array_slice($totalsArray, $start, $perPage);
                    $paginatedComparisonTotals = array_slice($comparisonArray, $start, $perPage);

                    // Response structure with pagination details
                    $response = [
                        'totals' => collect($paginatedTotals),
                        'comparisonTotals' => collect($paginatedComparisonTotals),
                        'currentQuarterPage' => $quarterPage,
                        'totalPages' => $totalPages,
                        'currentPage' => $page,
                        'quarterStartDate' => $startDate->toDateString(),
                        'quarterEndDate' => $endDate->toDateString(),
                        'comparisonQuarterStartDate' => $comparisonYear ? $comparisonStartDate->toDateString() : null,
                        'comparisonQuarterEndDate' => $comparisonYear ? $comparisonEndDate->toDateString() : null,
                    ];
                }
            }

            return  $response;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Helper function to calculate the initial start date based on the given year.
     */
    private function calculateStartDate($year, $initialStartDate2024)
    {
        if ($year >= 2024) {
            return $initialStartDate2024->copy()->addYears($year - 2024);
        } else {
            return $initialStartDate2024->copy()->subYears(2024 - $year);
        }
    }

    // Helper function to calculate top three proportions for a given field
    private function calculateTopProportions($query, $field)
    {
        $counts = $query->select($field, DB::raw("COUNT(*) as count"))
            ->groupBy($field)
            ->orderByDesc('count')
            ->limit(3)
            ->get();

        $total = $counts->sum('count');

        return $counts->map(function ($item) use ($total, $field) {
            return [
                'name' => $item->$field,
                'entries' => $item->count,
                'percentage' => $total > 0 ? round(($item->count / $total) * 100, 2) . '%' : '0%'
            ];
        });
    }

    public function getAnnualDataOld($queryBase, $page, $perPage, $years, $element = "location")
    {
        $year = $years[0];
        $comparisonYear = $years[1];
        try {
            $query = clone $queryBase;
            $query->whereYear('date', $year);

            $totalEntries = $query->count();

            $locations = $query->select(
                "$element",
                DB::raw("COUNT(*) as totals"),
                DB::raw("$totalEntries as total_entries"),
            )
                ->groupBy($element)
                ->orderBy($element, 'asc');

            if ($comparisonYear) {
                $locations = $locations->get()->keyBy("$element");
            } else {
                $locations = $locations
                    ->skip(($page - 1) * $perPage)
                    ->take($perPage)
                    ->get();
            }

            $response = [
                'totals' => $locations,
                'totalPages' => ceil($totalEntries / $perPage),
                'currentPage' => $page,
            ];

            // Fetch comparison data if the comparison year is provided
            if ($comparisonYear) {
                $comparisonQuery = clone $queryBase;
                $comparisonQuery->whereYear('date', $comparisonYear);

                $comparisonTotalEntries = $comparisonQuery->count();

                $comparisonLocations = $comparisonQuery->select(
                    "$element",
                    DB::raw("COUNT(*) as totals"),
                    DB::raw("$comparisonTotalEntries as total_entries"),
                )
                    ->groupBy($element)
                    ->orderBy($element, 'asc')
                    ->get()
                    ->keyBy($element);

                $allLocations = $locations->keys()->merge($comparisonLocations->keys())->unique()->sort();
                $totalsArray = [];
                $comparisonArray = [];

                foreach ($allLocations as $location) {
                    $totalsArray[] = [
                        "$element" => $location,
                        'totals' => $locations->has($location) ? $locations[$location]->totals : 0,
                        'total_entries' => $locations->has($location) ? $locations[$location]->total_entries : $totalEntries,
                        'year' => $year
                    ];

                    $comparisonArray[] = [
                        "$element" => $location,
                        'totals' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->totals : 0,
                        'total_entries' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->total_entries : $comparisonTotalEntries,
                        'year' => $comparisonYear
                    ];
                }

                // Calculate pagination
                $totalLocations = count($allLocations);
                $totalPages = ceil($totalLocations / $perPage);
                $start = ($page - 1) * $perPage;
                $end = $start + $perPage;

                $paginatedTotals = array_slice($totalsArray, $start, $perPage);
                $paginatedComparisonTotals = array_slice($comparisonArray, $start, $perPage);

                $response['totals'] = collect($paginatedTotals);
                $response['comparisonTotals'] = collect($paginatedComparisonTotals);
                $response['totalPages'] = $totalPages;
            }

            return $response;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getAnnualData($queryBase, $page, $perPage, $years, $locationIds = [], $causeIds = [], $sourceIds = [])
    {
        $year = $years[0];
        $comparisonYear = $years[1] ?? null;
        info("Comparision Year");
        info($comparisonYear);

        try {
            // Get the location names from the catalogs table based on location IDs
            [$locationNames, $causeNames, $sourceNames] = $this->getCatalogValues($locationIds, $causeIds, $sourceIds);

            // Clone the base query for the current year
            $query = clone $queryBase;
            $query->whereYear('date', $year);

            // If specific location names are passed, filter by them
            if (!empty($locationNames)) {
                $query->whereIn('location', $locationNames);
            } else {
                // No locations selected, bring all locations
                $query->whereNotNull('location');
            }

            if (!empty($causeNames)) {
                $query->whereIn('cause', $causeNames);
            }

            if (!empty($sourceNames)) {
                $query->whereIn('source', $sourceNames);
            }

            // Get total entries for pagination purposes
            $totalEntries = $query->count();

            $totalUniqueLocations = $query->select('location')
                ->groupBy('location')
                ->get()
                ->count();

            // Get locations and group by location, counting totals
            $locations = $query->select(
                'location',
                DB::raw("COUNT(*) as totals"),
                DB::raw("$totalEntries as total_entries")
            )
                ->groupBy('location')
                ->orderBy('location', 'asc');

            // Apply pagination to the query if no comparison year
            if (!$comparisonYear) {
                $locations = $locations
                    ->skip(($page - 1) * $perPage)
                    ->take($perPage)
                    ->get();
            } else {
                $locations = $locations->get()->keyBy('location');
            }

            $response = [
                'totals' => $locations,
                'totalPages' => ceil($totalUniqueLocations / $perPage),
                'currentPage' => $page,
            ];

            // If comparison year is provided, fetch comparison data
            if ($comparisonYear) {
                $comparisonQuery = clone $queryBase;
                $comparisonQuery->whereYear('date', $comparisonYear);

                // Apply the same filters for the comparison year
                if (!empty($locationNames)) {
                    $comparisonQuery->whereIn('location', $locationNames);
                }

                if (!empty($causeNames)) {
                    $comparisonQuery->whereIn('cause', $causeNames);
                }

                if (!empty($sourceNames)) {
                    $comparisonQuery->whereIn('source', $sourceNames);
                }

                // Get total entries for the comparison year
                $comparisonTotalEntries = $comparisonQuery->count();

                $comparisonLocations = $comparisonQuery->select(
                    'location',
                    DB::raw("COUNT(*) as totals"),
                    DB::raw("$comparisonTotalEntries as total_entries")
                )
                    ->groupBy('location')
                    ->orderBy('location', 'asc')
                    ->get()
                    ->keyBy('location');

                info('annual');
                info($locations);

                // Combine both sets of locations
                $allLocations = $locations->keys()->merge($comparisonLocations->keys())->unique()->sort();
                $totalsArray = [];
                $comparisonArray = [];

                // Create arrays for totals and comparison totals
                foreach ($allLocations as $location) {
                    $totalsArray[] = [
                        'location' => $location,
                        'totals' => $locations->has($location) ? $locations[$location]->totals : 0,
                        'total_entries' => $locations->has($location) ? $locations[$location]->total_entries : $totalEntries,
                        'year' => $year,
                    ];

                    $comparisonArray[] = [
                        'location' => $location,
                        'comparisonTotals' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->totals : 0,
                        'total_entries' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->total_entries : $comparisonTotalEntries,
                        'year' => $comparisonYear,
                    ];
                }

                // Calculate pagination for the combined locations
                $totalLocations = count($allLocations);
                $totalPages = ceil($totalLocations / $perPage);
                $start = ($page - 1) * $perPage;
                $end = $start + $perPage;

                // Slice the arrays for pagination
                $paginatedTotals = array_slice($totalsArray, $start, $perPage);
                $paginatedComparisonTotals = array_slice($comparisonArray, $start, $perPage);

                // Add comparison data to the response
                $response['totals'] = collect($paginatedTotals);
                $response['comparisonTotals'] = collect($paginatedComparisonTotals);
                $response['totalPages'] = $totalPages;
            }

            return $response;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function get13PeriodsOld($queryBase, $years)
    {
        $year = $years[0];
        $comparisonYear = $years[1];
        try {
            // Define the start date for 2024
            $initialStartDate2024 = Carbon::create(2024, 1, 3);

            // Calculate the initial start date for the given year
            if ($year >= 2024) {
                $yearsDifference = $year - 2024;
                $initialStartDate = $initialStartDate2024->copy()->addYears($yearsDifference);
            } else {
                $yearsDifference = 2024 - $year;
                $initialStartDate = $initialStartDate2024->copy()->subYears($yearsDifference);
            }

            // Adjust the initial start date to the correct day based on the 13 periods per year and leap years
            $daysDifference = ($yearsDifference * 52 * 7);
            if ($year >= 2024) {
                $initialStartDate = $initialStartDate2024->copy()->addDays($daysDifference);
            } else {
                $initialStartDate = $initialStartDate2024->copy()->subDays($daysDifference);
            }

            $periods = [];

            for ($i = 0; $i < 13; $i++) {
                $startDate = $initialStartDate->copy()->addWeeks($i * 4);
                $endDate = $startDate->copy()->addDays(27);

                $query = clone $queryBase;
                $totalEntries = $query->whereBetween('date', [$startDate->toDateString(), $endDate->toDateString()])->count();

                // Gather proportions for unfiltered fields
                $proportions = [
                    'location' => $this->calculateTopProportions($query, 'location'),
                    'source' => $this->calculateTopProportions($query, 'source'),
                    'cause' => $this->calculateTopProportions($query, 'cause'),
                ];

                $periods[] = [
                    'period' => 'P' . ($i + 1),
                    'start_date' => $startDate->toDateString(),
                    'end_date' => $endDate->toDateString(),
                    'totals' => $totalEntries,
                    'proportions' => $proportions
                ];
            }

            // If comparisonYear is passed, fetch the data for that year
            $comparisonPeriods = [];
            if ($comparisonYear) {
                $comparisonStartDate = clone $initialStartDate2024;

                // Adjust the initial start date for comparison year
                if ($comparisonYear >= 2024) {
                    $yearsDifference = $comparisonYear - 2024;
                    $comparisonStartDate = $comparisonStartDate->addYears($yearsDifference);
                } else {
                    $yearsDifference = 2024 - $comparisonYear;
                    $comparisonStartDate = $comparisonStartDate->subYears($yearsDifference);
                }

                $daysDifference = ($yearsDifference * 52 * 7);
                if ($comparisonYear >= 2024) {
                    $comparisonStartDate = $comparisonStartDate->addDays($daysDifference);
                } else {
                    $comparisonStartDate = $comparisonStartDate->subDays($daysDifference);
                }

                for ($i = 0; $i < 13; $i++) {
                    $compStartDate = $comparisonStartDate->copy()->addWeeks($i * 4);
                    $compEndDate = $compStartDate->copy()->addDays(27);

                    $comparisonQuery = clone $queryBase;
                    $totalCompEntries = $comparisonQuery->whereBetween('date', [$compStartDate->toDateString(), $compEndDate->toDateString()])->count();

                    $comparisonProportions = [
                        'location' => $this->calculateTopProportions($comparisonQuery, 'location'),
                        'source' => $this->calculateTopProportions($comparisonQuery, 'source'),
                        'cause' => $this->calculateTopProportions($comparisonQuery, 'cause'),
                    ];

                    $comparisonPeriods[] = [
                        'period' => 'P' . ($i + 1),
                        'start_date' => $compStartDate->toDateString(),
                        'end_date' => $compEndDate->toDateString(),
                        'totals' => $totalCompEntries,
                        'proportions' => $comparisonProportions
                    ];
                }

                return [
                    'periods' => $periods,
                    'comparisonPeriods' => $comparisonPeriods,
                ];
            }

            // Return both periods (main and comparison if applicable)
            return $periods;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    // public function get13Periods($queryBase, $years, $page = 1, $perPage, $locationIds = [])
    public function get13Periods($queryBase, $years, $periodPage = 1, $page = 1,  $perPage = 5, $locationIds = [], $causeIds = [], $sourceIds = [])
    {
        $year = $years[0];
        $comparisonYear = $years[1];
        try {
            // Define the start date for 2024
            $initialStartDate2024 = Carbon::create(2024, 1, 3);

            // Calculate the initial start date for the given year
            if ($year >= 2024) {
                $yearsDifference = $year - 2024;
                $initialStartDate = $initialStartDate2024->copy()->addYears($yearsDifference);
            } else {
                $yearsDifference = 2024 - $year;
                $initialStartDate = $initialStartDate2024->copy()->subYears($yearsDifference);
                // Handle the 28-day gap for non-leap years
                if (!$initialStartDate->isLeapYear()) {
                    $initialStartDate->subDay();  // Subtract a day for non-leap years
                }
            }

            // Adjust the start date based on the periodPage (pagination for periods)
            $startDate = $initialStartDate->copy()->addWeeks(($periodPage - 1) * 4);
            $endDate = $startDate->copy()->addDays(27);

            // Get catalog values for locations, causes, and sources
            list($locationNames, $causeNames, $sourceNames) = $this->getCatalogValues($locationIds, $causeIds, $sourceIds);

            // Main query for locations within this period
            $query = clone $queryBase;
            $query->whereBetween('date', [$startDate->toDateString(), $endDate->toDateString()]);

            // Apply location, cause, and source filters
            if (!empty($locationNames)) {
                $query->whereIn('location', $locationNames);
            }

            if (!empty($causeNames)) {
                $query->whereIn('cause', $causeNames);
            }

            if (!empty($sourceNames)) {
                $query->whereIn('source', $sourceNames);
            }

            // Fetch total locations count for this period (for location pagination)
            $totalEntries = $query->count();

            // Paginate locations within the period
            $locations = $query->select(
                'location',  // Fetch the location name directly
                DB::raw("COUNT(*) as totals"),
                DB::raw("$totalEntries as total_entries")
            )
                ->groupBy('location')
                ->orderBy('location', 'asc');

            // Apply pagination to the query if no comparison year
            if (!$comparisonYear) {
                $locations = $locations
                    ->skip(($page - 1) * $perPage)
                    ->take($perPage)
                    ->get();
            } else {
                $locations = $locations->get()->keyBy('location');
            }

            $response = [
                'totals' => $locations,
                'totalPages' => ceil($totalEntries / $perPage),
                'currentPage' => $page,
            ];

            // Fetch comparison data for the comparisonYear
            // $comparisonLocations = collect();
            if ($comparisonYear) {

                $comparisonStartDate = clone $initialStartDate2024;
                // Adjust the initial start date for comparison year
                if ($comparisonYear >= 2024) {
                    $yearsDifference = $comparisonYear - 2024;
                    $comparisonStartDate = $comparisonStartDate->addYears($yearsDifference);
                } else {
                    $yearsDifference = 2024 - $comparisonYear;
                    $comparisonStartDate = $comparisonStartDate->subYears($yearsDifference);

                    // Handle the leap year case for comparison
                    if (!$comparisonStartDate->isLeapYear()) {
                        $comparisonStartDate->subDay();  // Adjust for non-leap years
                    }
                }
                $comparisonStartDate = $comparisonStartDate->copy()->addWeeks(($periodPage - 1) * 4);
                $comparisonEndDate = $comparisonStartDate->copy()->addDays(27);


                $comparisonQuery = clone $queryBase;
                $comparisonQuery->whereBetween('date', [$comparisonStartDate->toDateString(), $comparisonEndDate->toDateString()]);

                // Apply same filters for the comparison year
                if (!empty($locationNames)) {
                    $comparisonQuery->whereIn('location', $locationNames);
                }

                if (!empty($causeNames)) {
                    $comparisonQuery->whereIn('cause', $causeNames);
                }

                if (!empty($sourceNames)) {
                    $comparisonQuery->whereIn('source', $sourceNames);
                }

                // Fetch comparison total count for this period
                $comparisonTotalEntries = $comparisonQuery->count();

                $comparisonLocations = $comparisonQuery->select(
                    'location',
                    DB::raw("COUNT(*) as totals"),
                    DB::raw("$comparisonTotalEntries as total_entries")
                )
                    ->groupBy('location')
                    ->orderBy('location', 'asc')
                    ->get()
                    ->keyBy('location');

                // Combine both the original and comparison locations into one response
                $allLocations = $locations->keys()->merge($comparisonLocations->keys())->unique()->sort();
                $totalsArray = [];
                $comparisonArray = [];

                // Create arrays for both totals and comparison totals
                foreach ($allLocations as $location) {
                    $totalsArray[] = [
                        'location' => $location,
                        'totals' => $locations->has($location) ? $locations[$location]->totals : 0,
                        'total_entries' => $locations->has($location) ? $locations[$location]->total_entries : $totalEntries,
                        'year' => $year,
                    ];

                    $comparisonArray[] = [
                        'location' => $location,
                        'comparisonTotals' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->totals : 0,
                        'total_entries' => $comparisonLocations->has($location) ? $comparisonLocations[$location]->total_entries : $comparisonTotalEntries,
                        'year' => $comparisonYear,
                    ];
                }

                // info('13periods');
                // info($totalsArray);
                // info($comparisonArray);

                // Total pages for periods and locations
                $totalPeriodPages = 13;  // Fixed as 13 periods per year
                $totalLocations = count($allLocations);
                $totalPages = ceil($totalLocations / $perPage);
                $start = ($page - 1) * $perPage;
                $paginatedTotals = array_slice($totalsArray, $start, $perPage);
                $paginatedComparisonTotals = array_slice($comparisonArray, $start, $perPage);

                // Prepare the response
                $response = [
                    'totals' => collect($paginatedTotals),
                    'comparisonTotals' => collect($paginatedComparisonTotals),
                    'totalPeriodPages' => $totalPeriodPages,  // Period pagination pages
                    'currentPeriodPage' => (int)$periodPage,
                    'totalPages' => $totalPages,  // Location pagination pages
                    'currentLocationPage' => (int)$page,
                    'periodStartDate' => $startDate->toDateString(),
                    'periodEndDate' => $endDate->toDateString(),
                    'comparisonPeriodStartDate' => $comparisonStartDate->toDateString(),
                    'comparisonPeriodEndDate' => $comparisonEndDate->toDateString(),
                ];
            }

            return $response;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    private function getCatalogValues($locationIds, $causeIds, $sourceIds)
    {
        $locationNames = [];
        $causeNames = [];
        $sourceNames = [];

        if (!empty($locationIds)) {
            // Get the location names from the catalogs table based on location IDs
            $locationNames = DB::table('catalogs')
                ->where('type', 'location')
                ->whereIn('id', $locationIds)
                ->pluck('value')
                ->toArray();
        }

        if (!empty($causeIds)) {
            // Get the cause names from the catalogs table based on cause IDs
            $causeNames = DB::table('catalogs')
                ->where('type', 'cause')
                ->whereIn('id', $causeIds)
                ->pluck('value')
                ->toArray();
        }

        if (!empty($sourceIds)) {
            // Get the cause names from the catalogs table based on cause IDs
            $sourceNames = DB::table('catalogs')
                ->where('type', 'source')
                ->whereIn('id', $sourceIds)
                ->pluck('value')
                ->toArray();
        }

        return [$locationNames, $causeNames, $sourceNames];
    }

    private function getCustomPeriodData($queryBase, $startPeriod, $endPeriod, $page, $perPage, $locationIds = [], $causeIds = [], $sourceIds = [])
    {
        // Check that start and end dates are provided
        if (!$startPeriod || !$endPeriod) {
            return response()->json(['error' => 'Invalid custom period. Start and end dates are required.'], 400);
        }

        // Clone the base query and apply date filtering
        $query = clone $queryBase;
        $query->whereBetween('date', [$startPeriod, $endPeriod]);

        // Apply filters for location, cause, and source, if provided
        list($locationNames, $causeNames, $sourceNames) = $this->getCatalogValues($locationIds, $causeIds, $sourceIds);
        if (!empty($locationNames)) {
            $query->whereIn('location', $locationNames);
        }
        if (!empty($causeNames)) {
            $query->whereIn('cause', $causeNames);
        }
        if (!empty($sourceNames)) {
            $query->whereIn('source', $sourceNames);
        }

        // Count the total entries for pagination purposes
        $totalEntries = $query->count();

        // Fetch the paginated results
        $locations = $query->select(
            'location',
            DB::raw("COUNT(*) as totals"),
            DB::raw("$totalEntries as total_entries")
        )
            ->groupBy('location')
            ->orderBy('location', 'asc')
            ->skip(($page - 1) * $perPage)
            ->take($perPage)
            ->get();

        // Prepare the response with pagination details
        return [
            'totals' => $locations,
            'totalPages' => ceil($totalEntries / $perPage),
            'currentPage' => $page,
            'customPeriodStartDate' => $startPeriod,
            'customPeriodEndDate' => $endPeriod
        ];
    }
}
