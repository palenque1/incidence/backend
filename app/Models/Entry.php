<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location',
        'source',
        'cause',
        'date',
        'injured',
        'reference',
        'case',
        'legal',
        'outpayment',
        'deductible',
        'loss',
        'restrict',
        'incurred',
        'shortdesc',
        'video',
        'medical',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function updates()
    {
        return $this->hasMany(Update::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date',
        'outpayment' => 'integer',
        'deductible' => 'integer',
        'video' => 'boolean',
        'medical' => 'boolean',
    ];
}

