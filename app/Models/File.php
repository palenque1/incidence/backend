<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'path',
        'mime_type',
        'size',
        'extension',
        'user_id', // Include this if you want to associate the file with a user
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the user that owns the file.
     */
    public function entry()
    {
        return $this->belongsTo(Entry::class);
    }

    // You can also define other methods here, for example, a method to get the full URL of the file
    public function getUrlAttribute()
    {
        return url('public/files/'.$this->path);
    }
}

