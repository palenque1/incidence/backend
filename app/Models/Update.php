<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    use HasFactory;

    protected $fillable = [
        'update',
        'date',
    ];

    public function entry()
    {
        return $this->belongsTo(Entry::class);
    }
}
