<?php

namespace App\Rules;

class EntryValidationRules
{
    public static function create()
    {
        return [
            'location' => 'required|string|max:255',
            'source' => 'required|string|max:255',
            'cause' => 'required|string|max:255',
            'date' => 'required|date',
            'reference' => 'required|string|max:255',
            'case' => 'required|string|max:255',
            'legal' => 'required|string|max:255',
            'injured' => 'nullable|string|min:0',
            'loss' => 'nullable|integer|min:0',
            'restrict' => 'nullable|integer|min:0',
            'incurred' => 'nullable|integer|min:0',
            'outpayment' => 'nullable|integer|min:0',
            'deductible' => 'nullable|integer|min:0',
            'shortdesc' => 'nullable|string',
            'video' => 'nullable|boolean',
            'medical' => 'nullable|boolean',
        ];
    }

    public static function update()
    {
        return [
            'location' => 'nullable|string|max:255',
            'source' => 'nullable|string|max:255',
            'cause' => 'nullable|string|max:255',
            'date' => 'nullable|date',
            'reference' => 'nullable|string|max:255',
            'case' => 'nullable|string|max:255',
            'legal' => 'nullable|string|max:255',
            'injured' => 'nullable|string|min:0',
            'loss' => 'nullable|integer|min:0',
            'restrict' => 'nullable|integer|min:0',
            'incurred' => 'nullable|integer|min:0',
            'outpayment' => 'nullable|integer|min:0',
            'deductible' => 'nullable|integer|min:0',
            'shortdesc' => 'nullable|string',
            'video' => 'nullable|boolean',
            'medical' => 'nullable|boolean',
        ];
    }

    public static function addUpdates()
    {
        return [
            'update' => 'string|max:255',
            'date' => 'date',
        ];
    }
}
