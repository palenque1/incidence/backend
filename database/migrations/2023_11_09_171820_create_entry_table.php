<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('location');
            $table->string('source');
            $table->string('cause');
            $table->date('date');
            $table->string('case');
            $table->string('legal');
            $table->string('reference');
            $table->integer('injured')->nullable();
            $table->integer('incurred')->nullable();
            $table->integer('loss')->nullable();
            $table->integer('restrict')->nullable();
            $table->integer('outpayment')->nullable();
            $table->integer('deductible')->nullable();
            $table->string('shortdesc')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entries');
    }
};
