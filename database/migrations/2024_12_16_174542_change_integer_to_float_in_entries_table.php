<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('entries', function (Blueprint $table) {
            $table->float('outpayment', 10, 2)->nullable()->change();
            $table->float('deductible', 10, 2)->nullable()->change();
            $table->float('loss', 10, 2)->nullable()->change();
            $table->float('restrict', 10, 2)->nullable()->change();
            $table->float('incurred', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('entries', function (Blueprint $table) {
            // Revert float columns back to integer
            $table->integer('outpayment')->nullable()->change();
            $table->integer('deductible')->nullable()->change();
            $table->integer('loss')->nullable()->change();
            $table->integer('restrict')->nullable()->change();
            $table->integer('incurred')->nullable()->change();
        });
    }
};
