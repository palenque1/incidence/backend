<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Catalog;
use Illuminate\Support\Facades\DB;

class CatalogSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('catalogs')->truncate();

        $locations = [
            'CASA PALENQUE INC',
            'CASA PALENQUE INC (Jr.)',
            'CASA PALENQUE INC (Del Mar)',
            'TP LAREDO NORTE, LLC',
            'TP LOOP 20, LLC',
            'TP BLANCO, LLC',
            'Pollos Expo Guadalupe, LLC (TP Corpus Christi)',
            'TACO PALENQUE COTULLA, LLC',
            'COMIDA RAPIDA TECNOLOGICA, LLC',
            'LAREDO COMIDAS, LLC',
            'TP MALL McALLEN, LLC',
            'TP BROWNSVILLE, LLC',
            'TP MISSION, LLC',
            'TP EDINBURG, LLC',
            'TP WESLACO, LLC',
            'TP PALMHURST, LLC',
            'TP HARLINGEN EXPWY LLC',
            'TP UT RGV, LLC',
            'TP SAN JUAN, LLC',
            'Pollos Expo Guadalupe, LLC (TP Bandera)',
            'TP ALIMENTOS, LLC',
            'TP ALIMENTOS, LLC (Sur)',
            'TP SAUNDERS, LLC',
            'TP SAT PARKDALE, LLC',
            'TP EVANS LLC',
            'TP COCINA, LLC (Operacion)',
            'TP COCINA, LLC (Produccion)',
            'TP COCINA, LLC (Bodega)',
            'TP COCINA, LLC (TORTILLAS)',
            'TORI CANAL, INC',
            'TORI LAREDO, INC',
            'POLLO PALENQUE McALLEN, LLC',
            'POLLO PALENQUE MISSION, LLC',
            'POLLO PALENQUE BROWNSVILLE, LLC',
            'PALENQUE BAR & GRILL, LLC',
            'PALENQUE GRILL HWY, LLC',
            'PALENQUE GRILL LA CANTERA, LLC',
            'PALENQUE GRILL McALLEN, LLC',
            'PALENQUE GRILL NOLANA, LLC',
        ];

        foreach ($locations as $locationName) {
            Catalog::create([
                'type' => 'location',
                'value' => $locationName,
            ]);
        }

        $causes = [
            'Auto Wreck',
            'Bad Practices',
            'Bad Equipment Practices',
            'Incidents',
            'Cuts',
            'Burns',
            'Facilities',
            'Slip & Falls',
            'Foods',
            'Miscelaneous',
            'Not Determined',
            'Personal',
            'Foreign Objects',
            'Stress',
        ];

        foreach ($causes as $cause) {
            Catalog::create([
                'type' => 'cause',
                'value' => $cause,
            ]);
        }

        $sources = [
            'Auto',
            'General Liability',
            'Workers Comp',
        ];

        foreach ($sources as $source) {
            Catalog::create([
                'type' => 'source',
                'value' => $source,
            ]);
        }
    }
}
