<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Specify the user's email or ID whose password you want to update
        $userEmail = 'zpena@palenquegroup.com'; // Change this to the specific user's email or ID
        $newPassword = 'Gman_1234'; // Change this to the new password

        // Hash the new password
        $hashedPassword = Hash::make($newPassword);

        // Update the user's password in the database
        DB::table('users')
            ->where('email', $userEmail) // Change 'email' to 'id' if you are using the user ID
            ->update(['password' => $hashedPassword]);

        $this->command->info('User password has been updated successfully.');
    }
}
