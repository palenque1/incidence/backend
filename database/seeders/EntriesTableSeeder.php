<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete related files and updates
        DB::table('files')->truncate();
        DB::table('updates')->truncate();
        DB::table('entries')->truncate();

        // Load data from CSV
        $csvFile = database_path('seeders/data/EntriesEn.csv');
        $data = array_map('str_getcsv', file($csvFile));
        $headers = array_shift($data);

        foreach ($data as $index => $row) {
            // Remove the first column (first element of $row)
            array_shift($row);

            // Trim all elements in the row and remove blank spaces
            $row = array_map('trim', $row);

            // Ensure row matches header length
            if (count($row) < count($headers)) {
                $row = array_pad($row, count($headers), null); // Pad with nulls if too short
            } elseif (count($row) > count($headers)) {
                $row = array_slice($row, 0, count($headers)); // Truncate if too long
            }

            // Match headers with row values
            $rowData = array_combine($headers, $row);

            // Convert location to uppercase
            if (isset($rowData['location'])) {
                $rowData['location'] = strtoupper($rowData['location']);
            }

            // Add the user_id
            $rowData['user_id'] = 1;

            // Fix "GLiability" -> "General Liability" in 'source' column
            if (isset($rowData['source']) && $rowData['source'] === 'GLiability') {
                $rowData['source'] = 'General Liability';
            }

            if (isset($rowData['source']) && $rowData['source'] === 'WorkersComp') {
                $rowData['source'] = 'Workers Comp';
            }

            // Validate and clean the 'date' column
            if (isset($rowData['date'])) {
                $date = date_create_from_format('m/d/Y', $rowData['date']);
                if ($date) {
                    $rowData['date'] = $date->format('Y-m-d'); // Convert to 'YYYY-MM-DD'
                } else {
                    $rowData['date'] = null; // Set invalid dates to null
                    \Log::warning("Invalid date format at row $index", ['data' => $row]);
                }
            }

            // Convert 'video' column to boolean
            if (isset($rowData['video'])) {
                $rowData['video'] = $rowData['video'] === '1.00' ? true : false;
            }

            // Convert numeric fields and clean commas
            $numericFields = ['outpayment', 'deductible', 'loss', 'restrict', 'incurred'];
            foreach ($numericFields as $field) {
                if (isset($rowData[$field])) {
                    $rowData[$field] = $rowData[$field] === '' ? 0 : (float)str_replace(',', '', $rowData[$field]);
                }
            }

            // Insert into the database
            DB::table('entries')->insert($rowData);
        }

        $this->command->info('Entries, Files, and Updates tables seeded successfully!');
    }
}
