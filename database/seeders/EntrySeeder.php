<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class EntrySeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $entries = [
           [
                'location' => 'New York',
                'source' => 'Online',
                'cause' => 'System Error',
                'date' => '2023-09-12',
                'reference' => 'NY123',
                'case' => 'IT Issue',
                'legal' => 'Awaiting Decision',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'System error led to data loss, awaiting legal decision.',
                'video' => false
            ],
            [
                'location' => 'Los Angeles',
                'source' => 'Email',
                'cause' => 'User Error',
                'date' => '2023-09-15',
                'reference' => 'LA123',
                'case' => 'User Training Issue',
                'legal' => 'Settled',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'User error due to lack of training, settled without legal issues.',
                'video' => false
            ],
            [
                'location' => 'Chicago',
                'source' => 'Referral',
                'cause' => 'Hardware Failure',
                'date' => '2023-10-01',
                'reference' => 'CH123',
                'case' => 'Critical Hardware Issue',
                'legal' => 'Under Investigation',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Major hardware failure causing operational downtime.',
                'video' => false
            ],
            [
                'location' => 'Houston',
                'source' => 'Walk-in',
                'cause' => 'Network Outage',
                'date' => '2023-10-05',
                'reference' => 'HO123',
                'case' => 'Network Connectivity Issue',
                'legal' => 'Claims Pending',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Outage caused by network issues, pending claims.',
                'video' => false
            ],
            [
                'location' => 'Phoenix',
                'source' => 'Call',
                'cause' => 'Scheduled Maintenance',
                'date' => '2023-10-10',
                'reference' => 'PH123',
                'case' => 'Regular Maintenance',
                'legal' => 'No Legal Action',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Planned maintenance completed with no issues.',
                'video' => false
            ],
            [
                'location' => 'San Francisco',
                'source' => 'Conference',
                'cause' => 'Information Breach',
                'date' => '2023-11-01',
                'reference' => 'SF123',
                'case' => 'Data Privacy',
                'legal' => 'Court Case',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Data breach during conference leading to litigation.',
                'video' => false
            ],
            [
                'location' => 'Atlanta',
                'source' => 'Social Media',
                'cause' => 'Software Bug',
                'date' => '2023-11-15',
                'reference' => 'Ref12345',
                'case' => 'Minor',
                'legal' => 'Pending Legal Review',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Software bug caused minor disruption.',
                'video' => false
            ],
            [
                'location' => 'Miami',
                'source' => 'Customer Support',
                'cause' => 'User Complaint',
                'date' => '2023-11-20',
                'reference' => 'Ref12346',
                'case' => 'Customer Complaint',
                'legal' => 'No Legal Action',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Customer complaint was resolved amicably.',
                'video' => false
            ],
            [
                'location' => 'Seattle',
                'source' => 'Forum',
                'cause' => 'Operational Issue',
                'date' => '2023-12-01',
                'reference' => 'Ref12347',
                'case' => 'Operational',
                'legal' => 'Under Review',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Operational issues caused temporary service outage.',
                'video' => false
            ],
            [
                'location' => 'Dallas',
                'source' => 'Advertisement',
                'cause' => 'Technical Glitch',
                'date' => '2023-12-05',
                'reference' => 'Ref12348',
                'case' => 'Technical',
                'legal' => 'No Legal Issue',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'A small technical glitch was quickly resolved.',
                'video' => false
            ],
            [
                'location' => 'Denver',
                'source' => 'Direct Mail',
                'cause' => 'Scheduled Update',
                'date' => '2023-12-10',
                'reference' => 'Ref12349',
                'case' => 'Maintenance',
                'legal' => 'None',
                'injured' => 30,
                'outpayment' => 2000,
                'deductible' => 200,
                'loss' => 300,
                'restrict' => 500,
                'incurred' => 600,
                'shortdesc' => 'Routine maintenance completed successfully.',
                'video' => false
            ],
        ];

        foreach ($entries as $entry) {
            User::find(1)->entry()->create($entry);
        }
    }
}
