<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Auth\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('App\Http\Controllers')->group(function () {
        Route::post('/register', [RegisteredUserController::class, 'store']);
        Route::post('/login', [AuthenticatedSessionController::class, 'store']);
        Route::post('/forgot-password', [PasswordResetLinkController::class, 'store']);
        Route::post('/reset-password', [NewPasswordController::class, 'store']);
        Route::get('/test', 'Controller@test');

        Route::middleware(["jwt.auth"])->group(function () {
            Route::post('/refresh-token', [AuthenticatedSessionController::class, 'refreshToken']);
            Route::patch('/change-password', [UserController::class, 'changePassword']);
            Route::patch('/change-rol/{userId}', [UserController::class, 'changeRol']);
            Route::post('/logout', [AuthenticatedSessionController::class, 'destroy']);
            Route::get('/users', [UserController::class, 'index']);
            Route::get('/{userId}/entry', 'EntryController@index');
            Route::post('/{userId}/entry', 'EntryController@store');
            Route::get('/{userId}/entry/{entryId}', 'EntryController@show');
            Route::patch('/{userId}/entry/{entryId}', 'EntryController@update');

            Route::post('/entry/{entryId}/update', 'EntryController@addUpdate');
            Route::get('/entry/{entryId}/update', 'EntryController@getUpdates');

            Route::get('/file/{entryId}', 'FileController@index');
            Route::post('/file/{entryId}', 'FileController@uploadFiles');
            Route::get('/file/download/{fileId}', 'FileController@downloadFile');
            Route::delete('/file/{fileId}', 'FileController@destroy');

            Route::get('/graph', 'VisualizationController@getDataForAll');
            Route::get('/graphQuarters', 'VisualizationController@getQuarters');
            Route::get('/graphAnnual', 'VisualizationController@getAnnualData');
            Route::get('/catalog-data', 'CatalogController@index');
            Route::post('/catalog-data', 'CatalogController@store');
            Route::delete('/catalog-data/{value}', 'CatalogController@destroy');
            Route::get('/locations-data', 'CatalogController@getLocations');
            Route::get('/causes-data', 'CatalogController@getCauses');
            Route::get('/sources-data', 'CatalogController@getSources');

            Route::get('/search', 'EntryController@search2');
        });
    });

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});
